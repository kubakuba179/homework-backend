CREATE TABLE public.users (
	id int8 NOT NULL,
	"name" varchar(255) NOT NULL,
	"password" varchar(255) NULL,
	username varchar(32) NULL,
	CONSTRAINT uk_r43af9ap4edm43mmtq01oddj6 UNIQUE (username),
	CONSTRAINT users_pkey PRIMARY KEY (id)
);