package homework.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

	private final Logger log = LogManager.getLogger(getClass());

	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "User not found.")
	@ExceptionHandler(UserNotFoundException.class)
	public void handleEntityNotFound(UserNotFoundException ex) {
		log.error("Entity not found.", ex);
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Request is not valid")
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public void handleConstraintViolationException(MethodArgumentNotValidException ex) {
		log.error("Request is not valid.", ex);
	}

	@ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED, reason = "Method not allowed")
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public void handleMetohodNotAllowedException(HttpRequestMethodNotSupportedException ex) {
		log.error("Method not supported occured on server", ex);
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Server error, please contact your admin.")
	@ExceptionHandler(Exception.class)
	public void handleAnyException(Exception ex) {
		log.error("Uncatched exception occured on server", ex);
	}
}
