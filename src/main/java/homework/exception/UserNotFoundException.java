package homework.exception;

public class UserNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 841328134596292506L;

	public UserNotFoundException() {
		super();
	}

	public UserNotFoundException(String message) {
		super(message);
	}
}
