package homework.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import homework.dao.UserRepository;
import homework.exception.UserNotFoundException;
import homework.model.User;

@Service
public class UserService {

	@Autowired
	private UserRepository userDao;

	public User findUser(Long id) {
		return userDao.findById(id).orElseThrow(() -> new UserNotFoundException());
	}

	public User addNewUser(User user) {
		return userDao.save(user);
	}

	public User editUser(Long id, User editedUser) {
		User userToEdit = userDao.findById(id).orElseThrow(() -> new UserNotFoundException());
		BeanUtils.copyProperties(editedUser, userToEdit, "id");
		return userDao.save(userToEdit);
	}

	public List<User> findAllUsers() {
		return userDao.findAll();
	}

	public void deleteUser(Long id) {
		User userToDelete = userDao.findById(id).orElseThrow(() -> new UserNotFoundException());
		userDao.delete(userToDelete);		
	}
}
