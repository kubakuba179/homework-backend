package homework.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import homework.dao.UserRepository;
import homework.model.User;

@Service
public class DBUserDetailsService implements UserDetailsService {
 
    @Autowired
    private UserRepository userDao;
 
    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userDao.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));        
        return new DBUserPrincipal(user);
    }
}