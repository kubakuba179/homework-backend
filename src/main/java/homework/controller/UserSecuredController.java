package homework.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import homework.service.UserService;

@RestController
@RequestMapping(value = "secured/user")
public class UserSecuredController {

	@Autowired
	private UserService userService;

	@RequestMapping(value= "{id}", method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable("id") Long id) {
		userService.deleteUser(id);
	}
	
}
