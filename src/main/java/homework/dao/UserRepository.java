package homework.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import homework.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

	Optional<User> findByUsername(String username);
}
